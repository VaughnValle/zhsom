<h1 align="center">Zero Hour Sound Overhaul Mod (ZHSOM) </h1>
<p align="center">
  <img src="https://img.shields.io/badge/MAINTAINED-YES-green?style=for-the-badge">
  <img src="https://img.shields.io/badge/LICENSE-MIT-blue?style=for-the-badge">
  <img src="https://img.shields.io/github/issues/VaughnValle/zhsom?style=for-the-badge">
   
  ZHSOM is an extensive sound overhaul mod for the game [Zero Hour.](https://store.steampowered.com/app/1359090/Zero_Hour)
</p>

[![ZHSOM](https://raw.githubusercontent.com/VaughnValle/demo/master/zhsom_header.png "ZHSOM Header")](https://store.steampowered.com/app/1359090/Zero_Hour)

## Table of Contents 
- [Installation](#installation)
- [Mod Feature List](#mod-feature-list)
  - [Weapon SFX](#weapon-sfx)
  - [Grenade SFX](#grenade-sfx)
  - [Other Miscellaneous SFX](#other-miscellaneous-sfx)

## Installation

This guide is for the latest ZHSOM build **1.0.0 release and later**.

> Latest release: **1.0.0** (February 15, 2021)

1. [Download this repository](https://github.com/VaughnValle/zhsom/archive/main.zip) as ZIP 
2. Extract the downloaded ZIP file to a folder using WinRAR, 7Zip, or the like
3. Copy `sounds.resource`, `sharedassets2.assets`, and `resources.assets` into your ***Zero Hour/Zero Hour_Data*** folder.
4. Overwrite the duplicate of the copied files.
5. Enjoy! :)

## Mod Feature List

This section lists the included sound improvements in the current build of ZHSOM.

### Weapon SFX

- Improved Kyanite SFX
- Improved Falcon SFX
- Improved G17 SFX
- Improved M4 SFX
  - M4 Chamber SFX
  - M4 Mag SFX
  - M4 Slap SFX
- Improved Mac-10 SFX
  - Improved Mac10 Mag SFX
  - Improved Mac10 charging handle SFX
- Improved MP5 SFX
  - MP5 Bolt Pull SFX
  - MP5 Slap SFX
- Improved SCAR 17 SFX
- Improved Shotgun SFX
- Improved Rattler SFX
  - Improved Rattler Charging Handle SFX
  - Improved Rattler Mag SFX

### Grenade SFX
- Improved frag grenade SFX
- Improved flashbang SFX
- Improved pin pull SFX

### Other Miscellaneous SFX
- Improved firemode change SFX
- Improved pistol SFX
  - Pistol chamber
  - Pistol mag eject and in
- Improved rifle SFX
  - Rifle empty Shot
  - Rifle mag SFX

